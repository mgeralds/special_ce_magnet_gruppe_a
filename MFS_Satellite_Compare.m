% variables
[swarmData, swarmInfo] = cdfread('SW_PPRO_MAGBSCAR1B_20170831T000000_20170831T235959_0408.cdf', 'CombineRecords', true);

fid = fopen('message.json');
opsData = jsondecode(char(fread(fid)'));
fclose(fid);

plot2D = 1; % 1 - 2D; 0 - 3D

% data conversion
convOpsData = convData([opsData.messwerte.gps_lat]', [opsData.messwerte.gps_long]', [opsData.messwerte.magn_scalar]');
[lat, long] = convertKugelToGeo(swarmData{1, 3}, swarmData{1, 4});
convSwarmData = convData(lat, long, swarmData{1, 6} / 1000000);

% data filter
data = dataFilter(convOpsData, convSwarmData);

% plot
if(plot2D)
    ax = plots2D(convOpsData, convSwarmData(data(:, 1), :), data(:, 2:end));
else
    ax = plot3D(convOpsData, convSwarmData(data(:, 1), :));
end

% functions
function [lat, long] = convertKugelToGeo(theta, phi)
    long = phi;
    lat = theta - 90;
end

function result = convData(lat, long, value)
    result = zeros(size(value,1), 3);
    for i = 1:size(value,1)
        result(i, :) = [lat(i), long(i), value(i)];
    end
end

function data = dataFilter(inData, filter)
    data = zeros(size(inData,1), 3);
    for messure = 1:size(inData,1)
        for i = 1:size(filter,1)
            latDiff = abs(filter(i, 1) - inData(messure, 1));
            longDiff = abs(filter(i, 2) - inData(messure, 2));
            if data(messure, 1) == 0 || (latDiff * 2)^2 + (longDiff)^2 < (data(messure, 2) * 2)^2 + (data(messure, 3))^2
                data(messure, :) = [i, latDiff, longDiff];
            end
        end
    end
end

% plot settings
function plotsettings(ax)
    for a = ax(1:end)
        legend(a, 'OPS-SAT', 'Swarm B', 'Location', 'eastoutside', 'FontSize', 14);
        set(a, 'XMinorTick', 'on', 'XGrid', 'on', 'YGrid', 'on', 'XMinorGrid', 'on', 'LineWidth', 1, 'FontSize', 14);
        axis(a, 'padded');
    end
end

function ax = plots2D(opsData, swarmData, locDiff)
    ax = [];
    tiledlayout(4,2)
    
    % 2D plots
    ax(end+1) = nexttile(1);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 3), 1:size(swarmData, 1), swarmData(:, 3), 'LineWidth', 1.5)
    title('Satellite messure compare in line style');
    xlabel('Messurement');
    ylabel('Magnetic field strength in T');

    ax(end+1) = nexttile(3);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 1), 1:size(swarmData, 1), swarmData(:, 1), 'LineWidth', 1.5)
    title('Latitude compare of selected messures in line style');
    xlabel('Messurement');
    ylabel('Latitude in °');

    ax(end+1) = nexttile(5);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 2), 1:size(swarmData, 1), swarmData(:, 2), 'LineWidth', 1.5)
    title('Longitude compare of selected messures in line style');
    xlabel('Messurement');
    ylabel('Longitude in °');

    ax(end+1) = nexttile(2);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 3), 'x', 1:size(swarmData, 1), swarmData(:, 3), 'o', 'MarkerSize', 8, 'LineWidth', 1.5)
    title('Satellite messure compare in scatter style');
    xlabel('Messurement');
    ylabel('Magnetic field strength in T');

    ax(end+1) = nexttile(4);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 1), 'x', 1:size(swarmData, 1), swarmData(:, 1), 'o', 'MarkerSize', 8, 'LineWidth', 1.5)
    title('Latitude compare of selected messures in scatter style');
    xlabel('Messurement');
    ylabel('Latitude in °');

    ax(end+1) = nexttile(6);
    plot(ax(end), 1:size(opsData, 1), opsData(:, 2), 'x', 1:size(swarmData, 1), swarmData(:, 2), 'o', 'MarkerSize', 8, 'LineWidth', 1.5)
    title('Longitude compare of selected messures in scatter style');
    xlabel('Messurement');
    ylabel('Longitude in °');

    ax(end+1) = nexttile(7);
    plot(ax(end), 1:size(locDiff, 1), locDiff(:, 1), 1:size(locDiff, 1), locDiff(:, 2), 'LineWidth', 1.5)
    title('Latitude and longitude difference');
    xlabel('Messurement');
    ylabel('Difference in °');

    ax(end+1) = nexttile(8);
    plot(ax(end), 1:size(locDiff, 1), locDiff(:, 1) / 1.80, 1:size(locDiff, 1), locDiff(:, 2) / 3.60, 'LineWidth', 1.5)
    title('Latitude and longitude difference');
    xlabel('Messurement');
    ylabel('Difference in %');
    
    % plot settings
    plotsettings(ax);
    for a = ax(end-1:end)
        legend(a, 'Latitude', 'Longitude');
    end
end

function ax = plot3D(opsData, swarmData)
    ax = [];
    tiledlayout(1,1)
    
    % 3D plot
    ax(end+1) = nexttile;
    plot3(ax(end), opsData(:, 1), opsData(:, 2), opsData(:, 3), swarmData(:, 1), swarmData(:, 2), swarmData(:, 3), 'LineWidth', 1.5)
    title('Messurement, longitude and latitude compare');
    xlabel('Latitude in °');
    ylabel('Longitude in °');
    zlabel('Magnetic field strength in T');
    
    % plot settings
    plotsettings(ax);
    set(ax(end), 'YMinorTick', 'on', 'ZMinorTick', 'on', 'YMinorGrid', 'on', 'ZMinorGrid', 'on')
end