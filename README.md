# Dokumentation von mission_magnfs Space Application (Gruppe A - Special CE SOSE2021)

## Voraussetzungen:

- Ubuntu-Rechner (VM ist auch ok!)

- Maven

- Bei Aufruf von... 

  `echo $JAVA_HOME`

  ...so sollte es zu sehen sein:
  `/usr/lib/jvm/java-8-openjdk-amd64`

- Bei Aufruf von...

  `java -version` 

  ...sollte es so zu sehen sein:
  `openjdk version "1.8.0_292"`
  `OpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~20.04-b10)`
  `OpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)`

## Ausführungsschritte mit Consumer Test Tool:
0. `source /etc/profile` ausführen, falls die Java-Einstellung dort geschrieben wurde
1. Das OPS-SAT-Framework-Repository clonen:
    `git clone https://github.com/esa/nanosat-mo-framework.git`

  Danach sollte ein Verzeichnis "nanosat-mo-framework" entstehen.

2. Der Ordner "mission_magnfs" in Verzeichnis "nanosat-mo-framework/sdk/examples/space" hinzufügen

3. Die vorhandene Datei "pom.xml" in Verzeichnis "nanosat-mo-framework/sdk/sdk-package" durch "pom.xml" aus diesem Repository ("SPECIAL_CE_MAGNET_GRUPPE_A") ersetzen.

4. Die vorhandene Datei "build.xml" in Verzeichnis "nanosat-mo-framework/sdk/sdk-package/antpkg" durch "build.xml" aus diesem Repository ("SPECIAL_CE_MAGNET_GRUPPE_A") ersetzen.

5. Wechsle in Verzeichnis "nanosat-mo-framework" `und` führe "`mvn clean install -X`" aus.

6. Wechsle in Verzeichnis "nanosat-mo-framework/sdk" und führe nochmal "`mvn clean install -X`" aus.

7. Wechsle in Verzeichnis "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/nmf/nanosat-mo-supervisor-sim" und führe "`./nanosat-mo-supervisor-sim.sh`"aus. **Danach sollte eine URI am Terminal ohne Java-Exceptions entstehen**.

   ![](./bild_rm/mag1.PNG)

8. In einem anderen Terminal-Fenster wechsle in Verzeichnis "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/nmf/consumer-test-tool" und führe "`./consumer-test-tool.sh`" aus. **Am Ende sollte eine GUI entstehen**.

9. In der GUI füge die URI, die am Terminal von "nanosat-mo-supervisor-sim" (7.Schritt) ausgegeben wird, in "Directory Service URI" ein und klicke "Fetch Information". **Am Ende sollte auf dem "Providers List" der Provider "nanosat-mo-supervisor" auftauchen. Klicke es und klicke "Connect to Selected Provider"**

   ![](./bild_rm/mag2.PNG)

10. Am "Apps Launcher Service" klicke die Zeile mit "mission_magnfs" und klicke "runApp". Warte bis die URI am Log-Fenster angezeigt wird.

    ![](./bild_rm/mag3.PNG)

11. Wechsle in Tab "Communication Settings (Directory)" und klicke "Fetch Information". Danach sollte die App "mission_nagnfs" am "Providers List" auftauchen.

    ![](./bild_rm/mag4.PNG)

12. Klicke "App: mission_magnfs" und klicke "Connect to Selected Provider"

    

13. Wechsle in Tab "Action Service", klicke die Zeile mit "StartMessung" und klicke "submitAction".

    ![](./bild_rm/mag5.PNG)

14. Klicke "Submit" am Popup...

    ![](./bild_rm/mag6.PNG)

    ...und klicke "Ok"

    ![](./bild_rm/mag7.PNG)

15. Warte, bis die Messungen durchgeführt wurden (Es kommt darauf an, wie es programmiert ist. Momentan beträgt es 30 Messungen, die jede 5 Sekunden durchgeführt werden)

16. Die Messungsergebnisse lassen sich in "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/mission_magnfs/messdaten" sehen. Die Messungsergebnisse sind in einer Textdatei zusammengefasst, die ein Array von JSON-Objekten (1 Objekt = 1 Messung) beinhaltet.

    ![](./bild_rm/mag8.PNG)


## Ausführungsschritte ohne CTT (direkt mit Bash-Datei):

0. `source /etc/profile` ausführen, falls die Java-Einstellung dort geschrieben wurde
1. Das OPS-SAT-Framework-Repository clonen:
   `git clone https://github.com/esa/nanosat-mo-framework.git`

  Danach sollte ein Verzeichnis "nanosat-mo-framework" entstehen.

2. Kopiere das Verzeichnis "sh_only/mission_magnfs" von diesem Repository ins Verzeichnis "nanosat-mo-framework/sdk/examples/space" vom geklonten OPS-SAT-Framework-Repository

3. Die vorhandene Datei "pom.xml" in Verzeichnis "nanosat-mo-framework/sdk/sdk-package" durch "pom.xml" aus diesem Repository ("SPECIAL_CE_MAGNET_GRUPPE_A") ersetzen.

4. Die vorhandene Datei "build.xml" in Verzeichnis "nanosat-mo-framework/sdk/sdk-package/antpkg" durch "build.xml" aus diesem Repository ("SPECIAL_CE_MAGNET_GRUPPE_A") ersetzen.

5. Wechsle ins Verzeichnis "nanosat-mo-framework" `und` führe "`mvn clean install -X`" aus.

6. Wechsle ins Verzeichnis "nanosat-mo-framework/sdk" und führe nochmal "`mvn clean install -X`" aus.

7. Wechsle ins Verzeichnis "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/nmf/nanosat-mo-supervisor-sim" und führe "`./nanosat-mo-supervisor-sim.sh`"aus. **Danach sollte eine URI am Terminal ohne Java-Exceptions entstehen**.

   ![](./bild_rm/mag1.PNG)

8. Wechsle in Verzeichnis "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/mission_magnfs" und ändere die Zeile mit "exec java..." in der Datei "start_mission_magnfs.sh" so, dass es "sh_only/start_mission_magnfs.sh" aus diesem Repository ähnelt. Die dabei stehende URI muss so geändert werden, dass es wie die URI aus Supervisor (siehe Schritt 8) gleich ist.

9. Führe "`./start_mission_magnfs.sh`"aus. So sollte es im Terminal zu sehen:

   ![](./bild_rm/sh_terminal.PNG)

10. Die Messungsergebnisse lassen sich in "nanosat-mo-framework/sdk/sdk-package/target/nmf-sdk-2.1.0-SNAPSHOT/home/mission_magnfs/messdaten" sehen. Die Messungsergebnisse sind in einer Textdatei zusammengefasst, die ein Array von JSON-Objekten (1 Objekt = 1 Messung) beinhaltet.

## Was gemessen werden:

- Datum und Zeit (Format: "yyyy-MM-dd_HH:mm:ss")
- Magnetische Feldstärke in x-, y- und z-Achsen (auch in Array + Skalare Größe)
- GPS-Länge, -Breite, und -Höhe

## Form der Messwerte:

`{"messwerte":` 
	`[`
    	`{`
        	`"datetime": "...".`
        	`"magn_x": ...,`
        	`"magn_y": ...,`
        	`"magn_z": ...,`
        	`"magn_arr" : [...,...,...],`
        	`"magn_scalar" : ...,`
        	`"gps_lat": ...,`
        	`"gps_long": ...,`
        	`"gps_alt": ...`
   	 `},` 
    	`{...},`
    	`{...},`
    	`... ,`
    	`{...}`
	`]`
`}`


## Referenzen:

https://github.com/esa/nanosat-mo-framework
https://nanosat-mo-framework.readthedocs.io/en/latest/sdk.html
