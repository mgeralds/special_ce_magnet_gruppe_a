/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esa.mo.nmf.apps;

import esa.mo.nmf.nanosatmoconnector.NanoSatMOConnectorImpl;
import esa.mo.nmf.MCRegistration;
import esa.mo.nmf.MonitorAndControlNMFAdapter;
import esa.mo.nmf.NMFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Math;
import org.ccsds.moims.mo.mal.MALException;
import org.ccsds.moims.mo.mal.MALInteractionException;
import org.ccsds.moims.mo.mal.provider.MALInteraction;
import org.ccsds.moims.mo.mal.structures.Attribute;
import org.ccsds.moims.mo.mal.structures.Identifier;
import org.ccsds.moims.mo.mal.structures.IdentifierList;
import org.ccsds.moims.mo.mal.structures.UInteger;
import org.ccsds.moims.mo.mal.structures.UOctet;
import org.ccsds.moims.mo.mal.structures.UShort;
import org.ccsds.moims.mo.mc.action.structures.ActionDefinitionDetails;
import org.ccsds.moims.mo.mc.action.structures.ActionDefinitionDetailsList;
import org.ccsds.moims.mo.mc.structures.ArgumentDefinitionDetails;
import org.ccsds.moims.mo.mc.structures.ArgumentDefinitionDetailsList;
import org.ccsds.moims.mo.mc.structures.AttributeValueList;
import org.ccsds.moims.mo.mc.structures.ConditionalConversionList;
import esa.mo.nmf.NMFInterface;


/**
 *
 * @author Gruppe A
 */
public class MagnSpaceApp {
    private final NanoSatMOConnectorImpl connector;
    private static final String START_MESSUNG = "StartMessung";
      private final int TOTAL_STAGES = 3;

    public MagnSpaceApp() {
        //this.connector = new NanoSatMOConnectorImpl();
        //this.connector.init(new mcAdapter());
        
    //final NanoSatMOConnectorImpl connector = new NanoSatMOConnectorImpl();
    //mcAdapter adapter = new mcAdapter(connector);
    //connector.init(adapter);
    //adapter.startAdcsAttitudeMonitoring();
    
    this.connector = new NanoSatMOConnectorImpl();
    this.connector.init(new mcAdapter(connector));
        
    }
    
    public static void main(final String args[]) throws Exception {
        MagnSpaceApp demo = new MagnSpaceApp();
    }

    
    public class mcAdapter extends MonitorAndControlNMFAdapter
    {
        public final NMFInterface nmf;
        
        public mcAdapter(final NMFInterface nmfProvider)
        {
            this.nmf = nmfProvider;
        }
        
        @Override
        public void initialRegistrations(MCRegistration registration)
        {
            registration.setMode(MCRegistration.RegistrationMode.DONT_UPDATE_IF_EXISTS);
        //----- Parameter... Leer lassen
        //----- Actions
        ActionDefinitionDetailsList actionDefs = new ActionDefinitionDetailsList();
        IdentifierList actionNames = new IdentifierList();

        ArgumentDefinitionDetailsList arguments1 = new ArgumentDefinitionDetailsList();
        {
            Byte rawType = Attribute._INTEGER_TYPE_SHORT_FORM;
            String rawUnit = "Output Data";
            ConditionalConversionList conditionalConversions = null;
            Byte convertedType = null;
            String convertedUnit = null;

            arguments1.add(new ArgumentDefinitionDetails(new Identifier("1"), null,
                rawType, rawUnit, conditionalConversions, convertedType, convertedUnit));
        }

        actionDefs.add(new ActionDefinitionDetails(
        "Durchfuehrung des Messungsdurchlaufs.",
        new UOctet((short) 0),
        new UShort(TOTAL_STAGES),
        arguments1
        ));
        actionNames.add(new Identifier(START_MESSUNG));
    
        registration.registerActions(actionNames, actionDefs);
        }
        //--- actionArrived
        @Override
        public UInteger actionArrived(Identifier name, AttributeValueList attributeValues,
      Long actionInstanceObjId, boolean reportProgress, MALInteraction interaction)
      {
          //ANPASSEN!
          int anz_zyklus = 30; //Anzahl der Messungszyklen
          int wartezeit = 5; //Wartezeit in Sekunden
          
          if (connector == null)
          {
              return new UInteger(0); 
          }
          
          if (START_MESSUNG.equals(name.getValue()))
          {
              try
              {
                  //Die Messung durchfuehren!
                  try
                  {
                      //Folder machen
                      final String folder = "messdaten";
                      File dir = new File(folder);
                      dir.mkdirs();
                      
                      //File prefix
                      Date date = new Date(System.currentTimeMillis());
                      Format format = new SimpleDateFormat("yyyyMMdd_HHmmss_");
                      final String timeNow = format.format(date);
                      final String filenamePrefix = folder + File.separator + timeNow;
                      
                      //Die eigentliche Datei
                      FileOutputStream fos = new FileOutputStream(filenamePrefix +"out_ops_sat.json");
                      
                      /*
                      String test = "Test";
                      byte[] test_byte = test.getBytes();
                      fos.write(test_byte);
                      String test2="Test2";
                      byte[] test_byte2= test2.getBytes();
                      fos.write(test_byte2);
                      */
                      /*
                      {
                        "messwerte":[
                                        {
                                            "datetime": "..." ,
                                            "magn_x": ... ,
                                            "magn_y": ... ,
                                            "magn_z" : ... ,
                                            "magn_arr" : [...,...,...],
                                            "magn_scalar" : ...,
                                            "gps_latitude": ... ,
                                            "gps_longitude": ... ,
                                            "gps_altitude" : ... 
                                        }, 
                                        {...},...
                                    ]
                      }
                      */
                      
                      //1. Anfang
                      String body_start = "{\"messwerte\":[";
                      byte[] start_byte = body_start.getBytes();
                      fos.write(start_byte);
                      
                      //2. Schleife fuer Erhalt von Messwerten
                      
                      //für Komma am Ende jeder Objekte
                      String komma =",";
                      byte[] kom_byte = komma.getBytes();
                      
                      for (int i =1; i<=anz_zyklus; i++)
                      {
                        //2a. Messwerte kriegen
                        String single_object = messungFunc(); 
                      
                        //2b2. in die Datei einfuegen!
                        byte[] obj_byte = single_object.getBytes();
                        fos.write(obj_byte);
                        
                        //1 bis (n-1)-ter Zyklus = Komma am Ende jedes Objekts + warten fuer bestimmte Dauer
                        if ((i >=1) && (i<=anz_zyklus-1))
                        {
                            //
                            fos.write(kom_byte);
                            //TimeUnit.SECONDS.sleep(wartezeit);

                            try { 
                                 Thread.sleep(wartezeit * 1000); 
                                } 
                            catch (InterruptedException ex) {
                                 Thread.currentThread().interrupt();
                                }

                        }
                      }

                      
                      
                      //3. Ende
                      String body_end = "]}";
                      byte[] end_byte = body_end.getBytes();
                      fos.write(end_byte);
                      
                      
                      //Schluss!
                      fos.flush();
                      fos.close();
                  }
                  catch (Exception e)
                  {
                      Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, e);
                  }
                  return null; // Success!
              }
              catch (Exception e) //MALInteractionException | MALException | IOException | NMFException e
              {
                  Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, e);
              }
          }
          
          return new UInteger(0);  // Action service not integrated
      }
       public String messungFunc()
       {
                 //1. Werte kriegen
                      
                      
                      //datetime
                      Date now_t = new Date(System.currentTimeMillis());
                      Format form = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                      String zeit_nun = form.format(now_t); 
                      
                      //magn_x -> in Tesla
                      Float magn_x = 0.0f; 
                      
                      try {
                        magn_x = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getX();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                        magn_x = null;
                          }        
                      
                      //magn_y -> in Tesla
                      Float magn_y = 0.0f;

                      try {
                        magn_y = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getY();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                        magn_y = null;
                          }                         
                      
                      //magn_z -> in Tesla
                      Float magn_z = 0.0f;

                      try {
                        magn_z = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getZ();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                            magn_z = null;
                          }
                      
                      //magn_scalar
                      Float magn_scalar =0.0f;
                      try {
                    	  magn_scalar = (float) Math.sqrt(magn_x*magn_x+magn_y*magn_y+magn_z*magn_z);
                      }
                      catch (Exception ex) 
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          magn_scalar = null;
                        }
                      
                      //gps_latitude
                      Float gps_lat = 0.0f;
                      try {
                          gps_lat= nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getLatitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          gps_lat = null;
                      }
                      
                      //gps_longitude
                      Float gps_long = 0.0f;
                      try {
                          gps_long = nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getLongitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                      {
                           Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                           gps_long = null;
                      }
                      
                      //gps_altitude
                      Float gps_alt = 0.0f;
                      try {
                          gps_alt = nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getAltitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex)
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          gps_alt = null;
                      }
                      
                      
                      
                    //2. Alle Messwerte in ein Objekt hinzufuegen
                      String obj_start= "{";
                      
                      String datetime_s = "\"datetime\":\"" + zeit_nun + "\",";
                      
                      String magn_x_s;
                      String magn_y_s;
                      String magn_z_s;
                      String magn_arr_s;
                      String magn_scalar_s;
                      String gps_latitude_s;
                      String gps_longitude_s;
                      String gps_altitude_s;
                      
                      String magn_x_temp;
                      String magn_y_temp;
                      String magn_z_temp;
                      
                      if (magn_x==null)
                      {
                           magn_x_s = "\"magn_x\":null,";
                           magn_x_temp ="null";
                      }
                      else
                      {
                           magn_x_s = "\"magn_x\":" + magn_x.toString() + ",";
                           magn_x_temp = magn_x.toString();
                      }
                      
                      if (magn_y==null)
                      {
                           magn_y_s ="\"magn_y\":null,";
                           magn_y_temp ="null";
                      }
                      else
                      {
                           magn_y_s = "\"magn_y\":" + magn_y.toString() + ",";
                           magn_y_temp = magn_y.toString();
                      }
                      
                      if (magn_z==null)
                      {
                           magn_z_s ="\"magn_z\":null,";
                           magn_z_temp ="null";
                      }
                      else
                      {
                           magn_z_s = "\"magn_z\":" + magn_z.toString() + ",";
                           magn_z_temp = magn_z.toString();
                      }
                      
                      
                      magn_arr_s = "\"magn_arr\":[" + magn_x_temp +"," + magn_y_temp + "," + magn_z_temp + "],";
                      
                      if (magn_scalar==null)
                      {
                    	  magn_scalar_s ="\"magn_scalar\":null,";
                      }
                      else
                      {
                    	  magn_scalar_s = "\"magn_scalar\":" + magn_scalar.toString() + ",";
                      }
                     
                      
                      if (gps_lat==null)
                      {
                           gps_latitude_s ="\"gps_lat\":null,";
                      }
                      else
                      {
                           gps_latitude_s = "\"gps_lat\":" + gps_lat.toString() + ",";
                      }
                      
                      if (gps_long==null)
                      {
                           gps_longitude_s ="\"gps_long\":null,";
                      }
                      else
                      {
                           gps_longitude_s = "\"gps_long\":" + gps_long.toString() + ",";
                      }
                      
                      if (gps_alt==null)
                      {
                           gps_altitude_s ="\"gps_alt\":null";
                      }
                      else
                      {
                           gps_altitude_s = "\"gps_alt\":" + gps_alt.toString();
                      }
                    
                      String obj_end= "}";
                      
                      String single_object = obj_start + datetime_s + magn_x_s + magn_y_s + magn_z_s+magn_arr_s+ magn_scalar_s+ gps_latitude_s + gps_longitude_s + gps_altitude_s + obj_end; 

                      
                      return single_object;
       }

    }
    
}
