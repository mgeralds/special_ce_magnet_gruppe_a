/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esa.mo.nmf.apps;

import esa.mo.nmf.nanosatmoconnector.NanoSatMOConnectorImpl;
import esa.mo.nmf.MonitorAndControlNMFAdapter;
import esa.mo.nmf.NMFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.Math;
import org.ccsds.moims.mo.mal.MALException;
import org.ccsds.moims.mo.mal.MALInteractionException;
import esa.mo.nmf.NMFInterface;
import java.util.concurrent.TimeUnit;



/**
 *
 * @author Gruppe A
 */
public class MagnSpaceApp {
    private final NanoSatMOConnectorImpl connector;
    public static MagnSpaceApp instance = new MagnSpaceApp();
    
    //public?
    private MagnSpaceApp() {
        connector = new NanoSatMOConnectorImpl();
    }
    
    
    public void start(){
        mcAdapter adapter = new mcAdapter(connector);
        connector.init(adapter);
        adapter.startMessung();
    }
           
    
    public static void main(final String args[]) throws Exception {
        MagnSpaceApp.instance.start();
    }

    
    public class mcAdapter extends MonitorAndControlNMFAdapter
    {
        //public final?
        private final NMFInterface nmf;
        
        private SafeThread messungThread = null;
        
        
        
        public mcAdapter(final NMFInterface nmfProvider)
        {
            this.nmf = nmfProvider;
        }
        
        
       public boolean startMessung()
       {
          if (messungThread==null || !messungThread.isAlive())
          {
              final MessungTask messungTask = new MessungTask();
              messungThread = new SafeThread(messungTask);
              messungThread.setDaemon(true);
              messungThread.start();
              Logger.getLogger(mcAdapter.class.getName()).log(Level.INFO, "The measurement is started.");
              //-------------------
              //30 Sekunden
              try { 
                    TimeUnit.SECONDS.sleep(30); //! Gesamte Dauer der Messung
                  } 
              catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                  }
              //Nach 30s = Stop!
              stopMessung();
              //-----------------------
          }
          else
          {
              Logger.getLogger(mcAdapter.class.getName()).log(Level.INFO, "The measurement is currently active.");
          }
          return true;
       }
       
       public void stopMessung()
       {
           messungThread.cancel();
           Logger.getLogger(mcAdapter.class.getName()).log(Level.INFO, "The measurement is stopped.");
           
       }
       
       private class MessungTask extends CancellableTask {
           MessungTask()
           {
               super(MessungTask.class.getName());
           }
           @Override
           public void doRun()
           {
               while(!cancelled)
               {
                    int anz_zyklus = 30; //! Anzahl der Messungszyklen
                    int wartezeit = 1; //! Wartezeit in Sekunden
                    if (connector == null)
                        {
                            return; 
                        }
          
                    try
                        {
                        //Die Messung durchfuehren!
                        try
                            {
                            //Folder machen
                            final String folder = "messdaten";
                            File dir = new File(folder);
                            dir.mkdirs();
                      
                            //File prefix
                            Date date = new Date(System.currentTimeMillis());
                            Format format = new SimpleDateFormat("yyyyMMdd_HHmmss_");
                            final String timeNow = format.format(date);
                            final String filenamePrefix = folder + File.separator + timeNow;
                      
                            //Die eigentliche Datei
                            FileOutputStream fos = new FileOutputStream(filenamePrefix +"out_ops_sat.json");
                      
                            //1. Anfang
                            String body_start = "{\"messwerte\":[";
                            byte[] start_byte = body_start.getBytes();
                            fos.write(start_byte);
                      
                            //2. Schleife fuer Erhalt von Messwerten
                      
                            //für Komma am Ende jeder Objekte
                            String komma =",";
                            byte[] kom_byte = komma.getBytes();
                      
                            for (int i =1; i<=anz_zyklus; i++)
                            {
                                //2a. Messwerte kriegen
                                String single_object = messungFunc(); 
                      
                                //2b2. in die Datei einfuegen!
                                byte[] obj_byte = single_object.getBytes();
                                fos.write(obj_byte);
                        
                                //1 bis (n-1)-ter Zyklus = Komma am Ende jedes Objekts + warten fuer bestimmte Dauer
                                if ((i >=1) && (i<=anz_zyklus-1))
                                {
                                //
                                fos.write(kom_byte);
                                //TimeUnit.SECONDS.sleep(wartezeit);

                                try { 
                                 Thread.sleep(wartezeit * 1000); 
                                } 
                                catch (InterruptedException ex) {
                                 Thread.currentThread().interrupt();
                                }

                        }
                      }

                      
                      
                            //3. Ende
                            String body_end = "]}";
                            byte[] end_byte = body_end.getBytes();
                            fos.write(end_byte);
                      
                      
                            //Schluss!
                            fos.flush();
                            fos.close();
                    }
                  catch (Exception e)
                  {
                      Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, e);
                      
                  }

                }
                catch (Exception e) //MALInteractionException | MALException | IOException | NMFException e
                    {
                  Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, e);
                  
                    }
               }
           }
       }
       
       public String messungFunc()
       {
                 //1. Werte kriegen
                      
                      
                      //datetime
                      Date now_t = new Date(System.currentTimeMillis());
                      Format form = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                      String zeit_nun = form.format(now_t); 
                      
                      //magn_x -> in Tesla
                      Float magn_x = 0.0f; 
                      
                      try {
                        magn_x = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getX();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                        magn_x = null;
                          }        
                      
                      //magn_y -> in Tesla
                      Float magn_y = 0.0f;

                      try {
                        magn_y = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getY();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                        magn_y = null;
                          }                         
                      
                      //magn_z -> in Tesla
                      Float magn_z = 0.0f;

                      try {
                        magn_z = ((float)Math.pow(10,-6))*nmf.getPlatformServices().getAutonomousADCSService().getStatus().getBodyElement0().getMagneticField().getZ();
                          } 
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                         {
                            Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                            magn_z = null;
                          }
                      
                      //magn_scalar
                      Float magn_scalar =0.0f;
                      try {
                    	  magn_scalar = (float) Math.sqrt(magn_x*magn_x+magn_y*magn_y+magn_z*magn_z);
                      }
                      catch (Exception ex) 
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          magn_scalar = null;
                        }
                      
                      //gps_latitude
                      Float gps_lat = 0.0f;
                      try {
                          gps_lat= nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getLatitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          gps_lat = null;
                      }
                      
                      //gps_longitude
                      Float gps_long = 0.0f;
                      try {
                          gps_long = nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getLongitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex) 
                      {
                           Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                           gps_long = null;
                      }
                      
                      //gps_altitude
                      Float gps_alt = 0.0f;
                      try {
                          gps_alt = nmf.getPlatformServices().getGPSService().getLastKnownPosition().getBodyElement0().getAltitude();
                      }
                      catch (NMFException | IOException | MALInteractionException | MALException ex)
                      {
                          Logger.getLogger(mcAdapter.class.getName()).log(Level.SEVERE, null, ex);
                          gps_alt = null;
                      }
                      
                      
                      
                    //2. Alle Messwerte in ein Objekt hinzufuegen
                      String obj_start= "{";
                      
                      String datetime_s = "\"datetime\":\"" + zeit_nun + "\",";
                      
                      String magn_x_s;
                      String magn_y_s;
                      String magn_z_s;
                      String magn_arr_s;
                      String magn_scalar_s;
                      String gps_latitude_s;
                      String gps_longitude_s;
                      String gps_altitude_s;
                      
                      String magn_x_temp;
                      String magn_y_temp;
                      String magn_z_temp;
                      
                      if (magn_x==null)
                      {
                           magn_x_s = "\"magn_x\":null,";
                           magn_x_temp ="null";
                      }
                      else
                      {
                           magn_x_s = "\"magn_x\":" + magn_x.toString() + ",";
                           magn_x_temp = magn_x.toString();
                      }
                      
                      if (magn_y==null)
                      {
                           magn_y_s ="\"magn_y\":null,";
                           magn_y_temp ="null";
                      }
                      else
                      {
                           magn_y_s = "\"magn_y\":" + magn_y.toString() + ",";
                           magn_y_temp = magn_y.toString();
                      }
                      
                      if (magn_z==null)
                      {
                           magn_z_s ="\"magn_z\":null,";
                           magn_z_temp ="null";
                      }
                      else
                      {
                           magn_z_s = "\"magn_z\":" + magn_z.toString() + ",";
                           magn_z_temp = magn_z.toString();
                      }
                      
                      
                      magn_arr_s = "\"magn_arr\":[" + magn_x_temp +"," + magn_y_temp + "," + magn_z_temp + "],";
                      
                      if (magn_scalar==null)
                      {
                    	  magn_scalar_s ="\"magn_scalar\":null,";
                      }
                      else
                      {
                    	  magn_scalar_s = "\"magn_scalar\":" + magn_scalar.toString() + ",";
                      }
                     
                      
                      if (gps_lat==null)
                      {
                           gps_latitude_s ="\"gps_lat\":null,";
                      }
                      else
                      {
                           gps_latitude_s = "\"gps_lat\":" + gps_lat.toString() + ",";
                      }
                      
                      if (gps_long==null)
                      {
                           gps_longitude_s ="\"gps_long\":null,";
                      }
                      else
                      {
                           gps_longitude_s = "\"gps_long\":" + gps_long.toString() + ",";
                      }
                      
                      if (gps_alt==null)
                      {
                           gps_altitude_s ="\"gps_alt\":null";
                      }
                      else
                      {
                           gps_altitude_s = "\"gps_alt\":" + gps_alt.toString();
                      }
                    
                      String obj_end= "}";
                      
                      String single_object = obj_start + datetime_s + magn_x_s + magn_y_s + magn_z_s+magn_arr_s+ magn_scalar_s+ gps_latitude_s + gps_longitude_s + gps_altitude_s + obj_end; 

                      
                      return single_object;
       }

    }
    
    public class SafeThread extends Thread {
	
	final CancellableTask task;
	
	public SafeThread(final CancellableTask task) {
		super(task, task.getName() + "_Thread");
		this.task = task;
	}
	
	public void cancel() {
		task.cancel();
	}
	
	public boolean isRunning() {
		return task.isRunning();
	}
	
	@Override
	public void interrupt() {
		task.cancel();
		super.interrupt();
	}
}

    public abstract class CancellableTask implements Runnable {

	protected boolean cancelled = false;
	protected boolean isRunning = false;
	
	protected final Logger logger;
	
	protected final String name;
	
	public String getName() {
		return name;
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	
	protected CancellableTask(String name) {
		this.name = name;
		this.logger = Logger.getLogger(name);
	}
	
	public void cancel() {
		logger.log(Level.INFO, "Cancel request acknowledged!");
		cancelled = true;
	}
	
	@Override
	public void run() {
		isRunning = true;
		doRun();
		isRunning = false;
		logger.log(Level.INFO, name + " stopped!");
    }
	
	public abstract void doRun();
}
    
    
}

