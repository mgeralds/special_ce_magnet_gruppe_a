#!/bin/sh

# NMF_LIB can be provided by the parent app (i.e. supervisor) or set locally
if [ -z "$NMF_LIB" ] ; then
    NMF_LIB=`cd ../nmf/lib > /dev/null; pwd`
fi

if [ -z "$NMF_HOME" ] ; then
    NMF_HOME=`cd ../nmf > /dev/null; pwd`
fi

if [ -z "$JAVA_OPTS" ] ; then
    JAVA_OPTS="-Xms32m -Xmx256m"
fi

LOCAL_LIB_PATH=`readlink -f lib`
LD_LIBRARY_PATH=$LOCAL_LIB_PATH:`cd ../nmf/lib > /dev/null; pwd`:$LD_LIBRARY_PATH

export JAVA_OPTS
export NMF_LIB
export NMF_HOME
export LD_LIBRARY_PATH

# Replaced with the main class name
MAIN_CLASS_NAME=esa.mo.nmf.apps.MagnSpaceApp

exec java $JAVA_OPTS \
  -Desa.mo.nmf.centralDirectoryURI=[URI aus Supervisor hier einfügen!] \
  -classpath "$NMF_LIB/*:lib/*:/usr/lib/java/*" \
  -Dnmf.platform.impl=${platform} \
  -Djava.util.logging.config.file="$NMF_HOME/logging.properties" \
  "$MAIN_CLASS_NAME" \
  "$@"
